package com.example.unitingromaniancommunity.Activities.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.unitingromaniancommunity.Activities.LoginActivity;
import com.example.unitingromaniancommunity.Activities.LoginDetails.CreateProfileActivity;
import com.example.unitingromaniancommunity.Activities.adapters.SlidingImage_Adapter;
import com.example.unitingromaniancommunity.Activities.models.ImageModel;
import com.example.unitingromaniancommunity.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

public class SliderActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rlSignin, rlSignup;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private ArrayList<ImageModel> imageModelArrayList;


    //
    private int[] myImageList = new int[]{R.drawable.sl1, R.drawable.sl2,
            R.drawable.sl3, R.drawable.sl4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);
        rlSignin = findViewById(R.id.signin_rel);
        rlSignup = findViewById(R.id.signup_rel);
        rlSignup.setOnClickListener(this);
        rlSignin.setOnClickListener(this);
        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();

        init();


    }

    private ArrayList<ImageModel> populateList() {

        ArrayList<ImageModel> list = new ArrayList<>();

        for (int i = 0; i < myImageList.length; i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);

        }

        return list;
    }

    private void init() {

        mPager = findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(SliderActivity.this, imageModelArrayList));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = imageModelArrayList.size();

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;


            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_rel:
                startActivity(new Intent(SliderActivity.this, CreateProfileActivity.class));
                finish();
                break;
            case R.id.signin_rel:
                startActivity(new Intent(SliderActivity.this, LoginActivity.class));
                finish();
                break;
        }
    }
}

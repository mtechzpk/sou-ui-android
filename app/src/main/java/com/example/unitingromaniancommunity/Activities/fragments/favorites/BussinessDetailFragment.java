package com.example.unitingromaniancommunity.Activities.fragments.favorites;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.Activities.adapters.ProfileAddPostAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileFriendsAdapter;
import com.example.unitingromaniancommunity.Activities.models.ProfileFriendsModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BussinessDetailFragment extends Fragment {
    private RecyclerView recyclerView,rvProfilePost;
    private ProfileFriendsAdapter pAdapter;
    private ProfileAddPostAdapter addpostAdapter;
    private ArrayList<ProfileFriendsModel> profileFriendsModels;
    View view;
    public BussinessDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_bussiness_detail, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return  view;
    }
    private void setPoemsName () {
        profileFriendsModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ProfileFriendsModel favFriendsTabModel = new ProfileFriendsModel();
            favFriendsTabModel.setUserName("Sheikh" + i);
            favFriendsTabModel.setPages(10);
            profileFriendsModels.add(favFriendsTabModel);
        }
    }

    private void initAdapter () {

        setPoemsName();
        pAdapter = new ProfileFriendsAdapter(getActivity(), profileFriendsModels);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}

package com.example.unitingromaniancommunity.Activities.fragments.feed;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.Activities.adapters.AllPostsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.MessagesTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.NotifiationTabAdapter;
import com.example.unitingromaniancommunity.Activities.models.AllPostsTabModel;
import com.example.unitingromaniancommunity.Activities.models.MessagesTabModel;
import com.example.unitingromaniancommunity.Activities.models.NotificationTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPostTab extends Fragment {
    private RecyclerView recyclerView;
    private AllPostsTabAdapter pAdapter;
    private ArrayList<AllPostsTabModel> allPostsTabModels;
    View view;

    public AllPostTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_post_tab, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        allPostsTabModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            AllPostsTabModel allPostsTabModel = new AllPostsTabModel();
            allPostsTabModel.setUserName("Fawad" + i);
            allPostsTabModel.setPages(10);
            allPostsTabModels.add(allPostsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new AllPostsTabAdapter(getActivity(), allPostsTabModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}

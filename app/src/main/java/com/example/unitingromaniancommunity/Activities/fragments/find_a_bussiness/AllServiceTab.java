package com.example.unitingromaniancommunity.Activities.fragments.find_a_bussiness;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.Activities.adapters.AllPostsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.FavBussinessTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.FavFriendsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.FriendsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.MessagesTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.NotifiationTabAdapter;
import com.example.unitingromaniancommunity.Activities.models.AllPostsTabModel;
import com.example.unitingromaniancommunity.Activities.models.FavBussinessTabModel;
import com.example.unitingromaniancommunity.Activities.models.FavFriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.FriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.MessagesTabModel;
import com.example.unitingromaniancommunity.Activities.models.NotificationTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllServiceTab extends Fragment {
    private RecyclerView recyclerView;
    private FavBussinessTabAdapter pAdapter;
    private ArrayList<FavBussinessTabModel> favBussinessTabModels;
    View view;

    public AllServiceTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_service_tab, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        favBussinessTabModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            FavBussinessTabModel favFriendsTabModel = new FavBussinessTabModel();
            favFriendsTabModel.setUserName("All Services" + i);
            favFriendsTabModel.setPages(10);
            favBussinessTabModels.add(favFriendsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new FavBussinessTabAdapter(getActivity(), favBussinessTabModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}

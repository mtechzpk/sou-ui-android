package com.example.unitingromaniancommunity.Activities.fragments.notification;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.Activities.adapters.MessagesTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.NotifiationTabAdapter;
import com.example.unitingromaniancommunity.Activities.models.MessagesTabModel;
import com.example.unitingromaniancommunity.Activities.models.NotificationTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesTab extends Fragment {
    private RecyclerView recyclerView;
    private MessagesTabAdapter pAdapter;
    private ArrayList<MessagesTabModel> messagesTabModels;
    View view;

    public MessagesTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_messages_tab, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        messagesTabModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            MessagesTabModel messagesTabModel = new MessagesTabModel();
            messagesTabModel.setUserName("Fawad" + i);
            messagesTabModel.setPages(10);
            messagesTabModels.add(messagesTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new MessagesTabAdapter(getActivity(), messagesTabModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}

package com.example.unitingromaniancommunity.Activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unitingromaniancommunity.Activities.activities.BussinessDetailActivity;
import com.example.unitingromaniancommunity.Activities.activities.MainActivity;
import com.example.unitingromaniancommunity.Activities.activities.YourFriendProfileActivity;
import com.example.unitingromaniancommunity.Activities.fragments.yourfriends.YourFriendProfileFragment;
import com.example.unitingromaniancommunity.Activities.models.FavFriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.YourFriendsTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;


public class YourFriendsTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    private ArrayList<YourFriendsTabModel> yourFriendsTabModels;

    public YourFriendsTabAdapter(Context context, ArrayList<YourFriendsTabModel> yourFriendsTabModels ) {
        this.context = context;
        this.yourFriendsTabModels = yourFriendsTabModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_find_friends, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
        holder1.llITem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, YourFriendProfileActivity.class);
                context.startActivity(intent);
//                MainActivity activity = (MainActivity) view.getContext();
//                Fragment myFragment = new YourFriendProfileFragment();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_frameLayout, myFragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return yourFriendsTabModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvFare;
        LinearLayout llITem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llITem = itemView.findViewById(R.id.llITem);

        }

        private void bind(int pos) {
            YourFriendsTabModel messagesTabModel = yourFriendsTabModels.get(pos);
            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
//        }
    }
//    public void loadFragment(Fragment fragment) {
//// create a FragmentManager
//        FragmentManager fm = context.getFragmentManager();
//// create a FragmentTransaction to begin the transaction and replace the Fragment
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//// replace the FrameLayout with new Fragment
//        fragmentTransaction.replace(R.id.home_frameLayout, fragment);
//        fragmentTransaction.commit(); // save the changes
//    }
//    public interface Callback {
//        void onItemClick(int pos);
//    }
}


package com.example.unitingromaniancommunity.Activities.interfaces;

public interface NetworkListener {
    public void onSuccess(String result);
    public void onError(String error);

}

package com.example.unitingromaniancommunity.Activities.fragments.favorites;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {
    View v;
    private TabLayout tabLayout;

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_favorite, container, false);
        initViews();
        loadFragment(new FriendsTab());
        initTabsView();
        return v;
    }

    public void initViews() {
        tabLayout = v.findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText("Friends"));
        tabLayout.addTab(tabLayout.newTab().setText("Bussiness"));
        tabLayout.addTab(tabLayout.newTab().setText("Events"));
        tabLayout.addTab(tabLayout.newTab().setText("Jobs"));


        //ivFilter = v.findViewById(R.id.ivFilter);
        //ivPremium = v.findViewById(R.id.ivPremium);
//        tabNearby = v.findViewById(R.id.tabNearby);
//        tabPopular = v.findViewById(R.id.tabpopular);
//        ivSetting.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_settingFragments2));
        //ivPremium.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_premiumFragment));


    }

    private void initTabsView() {
        tabLayout = v.findViewById(R.id.tab_layout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    loadFragment(new FriendsTab());
                } else if (tab.getPosition() == 1) {
                    loadFragment(new BussinessTab());
                }else if (tab.getPosition() == 2) {
                    loadFragment(new EventsTab());
                }else if (tab.getPosition() == 3) {
                    loadFragment(new JobTab());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    public void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.home_frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }
}

package com.example.unitingromaniancommunity.Activities.PostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;
import java.util.List;

public class PostJobActivity extends AppCompatActivity {
    private EditText business_name, job_title, hourly_rate, business_address,
            business_city, zip_code_optional, business_website, phone_number, contact_email, business_desc;
    private View business_img;
    private Spinner sp_job_type, sp_state, sp_required_exp, sp_language_spoken,
            sp_working_hours, sp_driver_license;
    private MultiSpinnerSearch sp_business_cat;
    private Button btn_post_job;
    ImageView  back_btn;
    private String b_name = "", b_job_title = "", rate_hourly = "", b_address = "", b_city = "", zip_code = "", b_web = "",
            phone_no = "", email = "", b_desc = "", job_type = "", state = "", experience = "", language = "", working_hours = "",
            business_cat = "", driving_license = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job);
        InitViews();
        AdaptersData();
        ClickViews();
    }

    private void ClickViews() {
        business_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: upload business image here
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_post_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b_name = business_name.getText().toString().trim();
                b_job_title = job_title.getText().toString().trim();
                rate_hourly = hourly_rate.getText().toString().trim();
                b_address = business_address.getText().toString().trim();
                b_city = business_city.getText().toString().trim();
                zip_code = zip_code_optional.getText().toString().trim();
                b_web = business_website.getText().toString().trim();
                phone_no = phone_number.getText().toString().trim();
                email = contact_email.getText().toString().trim();
                b_desc = business_desc.getText().toString().trim();

                if (!b_name.isEmpty()) {
                    if (!b_job_title.isEmpty()) {
                        if (!job_type.equals("Job Type")) {
                            if (!rate_hourly.isEmpty()) {
                                if (!b_city.isEmpty()) {
                                    if (!experience.equals("Required Experience")) {
                                        if (!language.equals("Language Spoken")) {
                                            if (!email.isEmpty()) {
                                                if (!b_desc.isEmpty()) {
                                                    //TODO: Hit Api Here
                                                    Toast.makeText(PostJobActivity.this, "Validated...", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    business_desc.setError("Detail Required");
                                                }
                                            } else {
                                                contact_email.setError("Email Required");
                                            }
                                        } else
                                            Toast.makeText(PostJobActivity.this, "Select Language", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(PostJobActivity.this, "Select Experience Required", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    business_city.setError("Field Required");
                                }
                            } else {
                                hourly_rate.setError("Field Required");
                            }
                        } else {
                            Toast.makeText(PostJobActivity.this, "Enter Job Type", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        job_title.setError("Field Required");
                    }
                } else {
                    business_name.setError("Field Required");
                }

            }
        });
    }

    private void InitViews() {
        business_name = findViewById(R.id.business_name);
        back_btn = findViewById(R.id.back_btn);
        job_title = findViewById(R.id.job_title);
        hourly_rate = findViewById(R.id.hourly_rate);
        business_address = findViewById(R.id.business_address);
        business_city = findViewById(R.id.city);
        zip_code_optional = findViewById(R.id.zip_code_optional);
        business_website = findViewById(R.id.business_website);
        phone_number = findViewById(R.id.phone_number);
        contact_email = findViewById(R.id.contact_email);
        business_desc = findViewById(R.id.business_desc);

        business_img = findViewById(R.id.business_img);

        btn_post_job = findViewById(R.id.btn_post_job);

        sp_job_type = findViewById(R.id.sp_job_type);
        sp_state = findViewById(R.id.sp_state);
        sp_required_exp = findViewById(R.id.sp_required_exp);
        sp_language_spoken = findViewById(R.id.sp_language_spoken);
        sp_working_hours = findViewById(R.id.sp_working_hours);
        sp_business_cat = findViewById(R.id.sp_business_cat);
        sp_driver_license = findViewById(R.id.sp_driver_license);

    }

    private void AdaptersData() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.job_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_job_type.setAdapter(adapter);
        sp_job_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                job_type = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"job_type",job_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_hours = ArrayAdapter.createFromResource(this,
                R.array.working_hours_array, android.R.layout.simple_spinner_item);
        adapter_hours.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_working_hours.setAdapter(adapter_hours);
        sp_working_hours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                working_hours = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"working_hours",working_hours);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_license = ArrayAdapter.createFromResource(this,
                R.array.driver_license_array, android.R.layout.simple_spinner_item);
        adapter_license.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_driver_license.setAdapter(adapter_license);
        sp_driver_license.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                driving_license = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"driving_license",driving_license);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_exp = ArrayAdapter.createFromResource(this,
                R.array.required_exp_array, android.R.layout.simple_spinner_item);
        adapter_exp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_required_exp.setAdapter(adapter_exp);
        sp_required_exp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                experience = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"experience",experience);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_lang = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_item);
        adapter_lang.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_language_spoken.setAdapter(adapter_lang);
        sp_language_spoken.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                language = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"language",language);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_state = ArrayAdapter.createFromResource(this,
                R.array.state_array, android.R.layout.simple_spinner_item);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_state.setAdapter(adapter_state);
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"state",state);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> business_cat_list = new ArrayList<>();
        business_cat_list.add("B Cat 1");
        business_cat_list.add("B Cat 2");
        business_cat_list.add("B Cat 3");
        business_cat_list.add("B Cat 4");
        business_cat_list.add("B Cat 5");
        business_cat_list.add("B Cat 6");

        final List<KeyPairBoolData> sportsArray0 = new ArrayList<>();
        for (int i = 0; i < business_cat_list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(business_cat_list.get(i));
            h.setSelected(false);
            sportsArray0.add(h);
        }
        sp_business_cat.setItems(sportsArray0, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                ArrayList<String> getSports = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        Toast.makeText(PostJobActivity.this, items.get(i).getName() + " : " + items.get(i).isSelected(), Toast.LENGTH_SHORT).show();
                        getSports.add(items.get(i).getName());
                    }
                }
            }
        });
    }
}

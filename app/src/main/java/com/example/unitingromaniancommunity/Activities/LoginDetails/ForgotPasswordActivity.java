package com.example.unitingromaniancommunity.Activities.LoginDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.unitingromaniancommunity.Activities.PostActivities.PostEventActivity;
import com.example.unitingromaniancommunity.Activities.PostActivities.PostJobActivity;
import com.example.unitingromaniancommunity.R;

public class ForgotPasswordActivity extends AppCompatActivity {
    private EditText user_email;
    private ImageView back_btn;
    private String email;
    private Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        user_email = findViewById(R.id.user_email);
        back_btn = findViewById(R.id.back_btn);
        btn_submit = findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = user_email.getText().toString().trim();
                if(!email.isEmpty()){

                    //hit forgot password api here
                    Toast.makeText(ForgotPasswordActivity.this, email, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ForgotPasswordActivity.this, PostJobActivity.class));
                }else {
                    user_email.setError("Email Required");
                }

            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

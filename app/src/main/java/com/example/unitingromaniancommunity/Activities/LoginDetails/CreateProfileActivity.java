package com.example.unitingromaniancommunity.Activities.LoginDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unitingromaniancommunity.Activities.Extras.Utilities;
import com.example.unitingromaniancommunity.Activities.LoginActivity;
import com.example.unitingromaniancommunity.R;

public class CreateProfileActivity extends AppCompatActivity {

    private Spinner sp_age, sp_gender, sp_state, sp_marital_status;
    private EditText user_city, first_name, last_name, user_bio, user_email, create_password, confirm_password;
    private View profile_img;
    private TextView profile_img_name;
    private Button btn_sign_up, btn_sign_in;
    private String age = "", gender = "", state = "", marital_status = "", f_name = "", l_name = "",
            city = "", bio = "", email = "", password = "", c_password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        InitViews();
        AdaptersData();
        ClickViews();

    }

    private void InitViews() {
        sp_age = findViewById(R.id.sp_age);
        sp_gender = findViewById(R.id.sp_gender);
        sp_state = findViewById(R.id.sp_state);
        sp_marital_status = findViewById(R.id.sp_marital_status);

        profile_img_name = findViewById(R.id.profile_img_name);

        user_city = findViewById(R.id.city);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        user_bio = findViewById(R.id.bio);
        user_email = findViewById(R.id.user_email);
        create_password = findViewById(R.id.create_password);
        confirm_password = findViewById(R.id.confirm_password);

        profile_img = findViewById(R.id.profile_img);

        btn_sign_up = findViewById(R.id.btn_sign_up);
        btn_sign_in = findViewById(R.id.btn_sign_in);
    }

    private void ClickViews() {

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CreateProfileActivity.this, LoginActivity.class));
                finish();
            }
        });

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                f_name = first_name.getText().toString().trim();
                l_name = last_name.getText().toString().trim();
                city = user_city.getText().toString().trim();
                bio = user_bio.getText().toString().trim();
                email = user_email.getText().toString().trim();
                password = create_password.getText().toString().trim();
                c_password = confirm_password.getText().toString().trim();

                Intent intent = new Intent(CreateProfileActivity.this, UpdateProfileActivity.class);
                startActivity(intent);

                if (!f_name.isEmpty()) {
                    if (!l_name.isEmpty()) {
                        if (!age.equals("Age")) {
                            if (!gender.equals("Gender")) {
                                if (!city.isEmpty()) {
                                    if (!state.equals("State")) {
                                        if (!marital_status.equals("Marital Status")) {
                                            if (!bio.isEmpty()) {
                                                if (!email.isEmpty()) {
                                                    if (!password.isEmpty()) {
                                                        if (!c_password.isEmpty()) {
                                                            if (!password.equals(c_password)) {

                                                                //Hit Api Here
                                                                Toast.makeText(CreateProfileActivity.this, "Validated", Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                confirm_password.setError("Password not matched");
                                                            }
                                                        } else {
                                                            confirm_password.setError("Field Required");
                                                        }
                                                    } else {
                                                        create_password.setError("Field Required");
                                                    }
                                                } else {
                                                    user_email.setError("Field Required");
                                                }
                                            } else {
                                                user_bio.setError("Field Required");
                                            }
                                        } else {
                                            Toast.makeText(CreateProfileActivity.this, "Marital Status Required", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(CreateProfileActivity.this, "State Required", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    user_city.setError("City Required");
                                }
                            } else {
                                Toast.makeText(CreateProfileActivity.this, "Gender Required", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(CreateProfileActivity.this, "Age Required", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        last_name.setError("Last Name Required");
                    }
                } else {
                    last_name.setError("First Name Required");
                }
            }
        });

    }

    private void AdaptersData() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.age_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_age.setAdapter(adapter);
        sp_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                age = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"age",age);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_gender = ArrayAdapter.createFromResource(this,
                R.array.gender_array, android.R.layout.simple_spinner_item);
        adapter_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_gender.setAdapter(adapter_gender);
        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"gender",gender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_state = ArrayAdapter.createFromResource(this,
                R.array.state_array, android.R.layout.simple_spinner_item);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_state.setAdapter(adapter_state);
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"state",state);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_marital_status = ArrayAdapter.createFromResource(this,
                R.array.marital_status_array, android.R.layout.simple_spinner_item);
        adapter_marital_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_marital_status.setAdapter(adapter_marital_status);
        sp_marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                marital_status = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"marital_status",marital_status);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}

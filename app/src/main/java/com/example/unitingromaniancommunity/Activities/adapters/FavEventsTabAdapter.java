package com.example.unitingromaniancommunity.Activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unitingromaniancommunity.Activities.activities.EventDetailActivity;
import com.example.unitingromaniancommunity.Activities.activities.JobDetailActivity;
import com.example.unitingromaniancommunity.Activities.models.FavEventsTabModel;
import com.example.unitingromaniancommunity.Activities.models.FriendsTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;


public class FavEventsTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FavEventsTabModel> favEventsTabModels;

    public FavEventsTabAdapter(Context context, ArrayList<FavEventsTabModel> favEventsTabModels) {
        this.context = context;
        this.favEventsTabModels = favEventsTabModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_fav_events_tab, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
        holder1.llITem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, EventDetailActivity.class);
                context.startActivity(intent);
//                MainActivity activity = (MainActivity) view.getContext();
//                Fragment myFragment = new BussinessDetailFragment();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, myFragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favEventsTabModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvFare;
        LinearLayout llITem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llITem = itemView.findViewById(R.id.llITem);

        }

        private void bind(int pos) {
            FavEventsTabModel messagesTabModel = favEventsTabModels.get(pos);
            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

//    public interface Callback {
//        void onItemClick(int pos);
//    }


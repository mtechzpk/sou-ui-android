package com.example.unitingromaniancommunity.Activities.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.unitingromaniancommunity.Activities.LoginDetails.CreateBusinessProfileActivity;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileAddPostAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileFriendsAdapter;
import com.example.unitingromaniancommunity.Activities.models.ProfileFriendsModel;
import com.example.unitingromaniancommunity.R;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;

public class BussinessDetailActivity extends AppCompatActivity {
    ExpandableRelativeLayout expandableLayout1;
    private RecyclerView recyclerView,rvProfilePost;
    private ProfileFriendsAdapter pAdapter;
    private ProfileAddPostAdapter addpostAdapter;
    private ArrayList<ProfileFriendsModel> profileFriendsModels;
    Button bEditBussiness;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bussiness_detail);
        recyclerView =findViewById(R.id.rv_poemName);
        bEditBussiness =findViewById(R.id.bEditBussiness);
        bEditBussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BussinessDetailActivity.this, CreateBusinessProfileActivity.class));

            }
        });
        initAdapter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void setPoemsName () {
        profileFriendsModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ProfileFriendsModel favFriendsTabModel = new ProfileFriendsModel();
            favFriendsTabModel.setUserName("Sheikh" + i);
            favFriendsTabModel.setPages(10);
            profileFriendsModels.add(favFriendsTabModel);
        }
    }

    private void initAdapter () {

        setPoemsName();
        pAdapter = new ProfileFriendsAdapter(BussinessDetailActivity.this, profileFriendsModels);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(BussinessDetailActivity.this, 3, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pAdapter);
    }
        public void expandableButton1(View view) {
            expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
            expandableLayout1.toggle(); // toggle expand and collapse
            expandableLayout1.expand();
// collapse
            expandableLayout1.collapse();
        }
    }


package com.example.unitingromaniancommunity.Activities.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.unitingromaniancommunity.R;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

public class JobDetailActivity extends AppCompatActivity {
    ExpandableRelativeLayout expandableLayout1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
    }
    public void expandableButton1(View view) {
        expandableLayout1 = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout1);
        expandableLayout1.toggle(); // toggle expand and collapse
        expandableLayout1.expand();
// collapse
        expandableLayout1.collapse();
    }
}

package com.example.unitingromaniancommunity.Activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unitingromaniancommunity.Activities.models.AllPostsTabModel;
import com.example.unitingromaniancommunity.Activities.models.MessagesTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;


public class AllPostsTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<AllPostsTabModel> allPostsTabModels;

    public AllPostsTabAdapter(Context context, ArrayList<AllPostsTabModel> allPostsTabModels) {
        this.context = context;
        this.allPostsTabModels = allPostsTabModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_post_tab, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return allPostsTabModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvAddress,tvTitle;
        LinearLayout llITem,llShare;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            llShare = itemView.findViewById(R.id.llShare);
            llITem = itemView.findViewById(R.id.llITem);

        }

        private void bind(int pos) {
            AllPostsTabModel messagesTabModel = allPostsTabModels.get(pos);
            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

//    public interface Callback {
//        void onItemClick(int pos);
//    }


package com.example.unitingromaniancommunity.Activities.PaymentActivity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.unitingromaniancommunity.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class PaymentInformationActivity extends AppCompatActivity {
    private ImageView back_btn, help;
    private Spinner ad_days, sp_card_type;
    private EditText card_name, card_number, expiry_date, zip_code, confirm_password, cvv;
    private Button btn_authorize;
    private String number_ad_days = "", card_type = "", c_name = "", c_number = "",
            c_expiry_date = "", c_zip_code = "", c_cvv = "", c_password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_information);
        InitViews();
        AdaptersData();
        ClickViews();

    }

    private void InitViews() {
        back_btn = findViewById(R.id.back_btn);
        help = findViewById(R.id.help);

        ad_days = findViewById(R.id.ad_days);
        sp_card_type = findViewById(R.id.sp_card_type);

        cvv = findViewById(R.id.cvv);
        card_name = findViewById(R.id.card_name);
        card_number = findViewById(R.id.card_number);
        expiry_date = findViewById(R.id.expiry_date);
        zip_code = findViewById(R.id.zip_code);
        confirm_password = findViewById(R.id.confirm_password);

        btn_authorize = findViewById(R.id.btn_authorize);
    }

    private void ClickViews() {
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentInformationActivity.this).create();
                alertDialog.setTitle("CVV");
                alertDialog.setMessage("Message will be displayed here");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
        });

        btn_authorize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c_name = card_name.getText().toString().trim();
                c_number = card_number.getText().toString().trim();
                c_expiry_date = expiry_date.getText().toString().trim();
                c_cvv = cvv.getText().toString().trim();
                c_zip_code = zip_code.getText().toString().trim();
                c_password = confirm_password.getText().toString().trim();

                if (!number_ad_days.equals("Number Days to Run Job Ad")) {
                    if (!card_type.equals("Card Type")) {
                        if (!c_name.isEmpty()) {
                            if (!c_number.isEmpty()) {
                                if (!c_expiry_date.isEmpty()) {
                                    if (!c_cvv.isEmpty()) {
                                        if (!c_zip_code.isEmpty()) {
                                            if (!c_password.isEmpty()) {
                                                //TODO: Api here
                                                Toast.makeText(PaymentInformationActivity.this, "Validated Everything.", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            zip_code.setError("Field Required");
                                        }
                                    } else {
                                        cvv.setError("Field Required");
                                    }
                                } else {
                                    expiry_date.setError("Field Required");
                                }
                            } else {
                                card_number.setError("Field Required");
                            }
                        } else {
                            card_name.setError("Field Required");
                        }
                    } else {
                        Toast.makeText(PaymentInformationActivity.this, "Select Card Type", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PaymentInformationActivity.this, "Select Number Days", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void AdaptersData() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.days_ad_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ad_days.setAdapter(adapter);
        ad_days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                number_ad_days = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"number_ad_days ",number_ad_days );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_card_type = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_item);
        adapter_card_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_card_type.setAdapter(adapter_card_type);
        sp_card_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                card_type = parent.getItemAtPosition(position).toString();
//                Utilities.saveString(CreateProfileActivity.this,"card_type",card_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

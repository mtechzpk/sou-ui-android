package com.example.unitingromaniancommunity.Activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unitingromaniancommunity.Activities.models.FindFriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.YourFriendsTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;


public class FindFriendsTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FindFriendsTabModel> findFriendsTabModels;

    public FindFriendsTabAdapter(Context context, ArrayList<FindFriendsTabModel> findFriendsTabModels) {
        this.context = context;
        this.findFriendsTabModels = findFriendsTabModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_find_friends_tab, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return findFriendsTabModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvFare;
        LinearLayout llITem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);

        }

        private void bind(int pos) {
            FindFriendsTabModel messagesTabModel = findFriendsTabModels.get(pos);
            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

//    public interface Callback {
//        void onItemClick(int pos);
//    }


package com.example.unitingromaniancommunity.Activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.unitingromaniancommunity.Activities.models.NotificationTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;


public class NotifiationTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<NotificationTabModel> poems;

    public NotifiationTabAdapter(Context context, ArrayList<NotificationTabModel> poems) {
        this.context = context;
        this.poems = poems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_tab, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return poems.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvLikedPost,tvDate;
        LinearLayout llITem,llDelete;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvLikedPost = itemView.findViewById(R.id.tvLikedPost);
            tvDate = itemView.findViewById(R.id.tvDate);
            llDelete = itemView.findViewById(R.id.llDelete);
            llITem = itemView.findViewById(R.id.llITem);

        }

        private void bind(int pos) {
            NotificationTabModel poem = poems.get(pos);
            tvName.setText(poem.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

//    public interface Callback {
//        void onItemClick(int pos);
//    }


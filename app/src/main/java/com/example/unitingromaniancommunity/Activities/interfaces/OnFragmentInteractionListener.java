package com.example.unitingromaniancommunity.Activities.interfaces;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(String title);
}

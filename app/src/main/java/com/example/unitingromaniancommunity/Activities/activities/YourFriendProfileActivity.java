package com.example.unitingromaniancommunity.Activities.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.unitingromaniancommunity.Activities.adapters.ProfileAddPostAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileFriendsAdapter;
import com.example.unitingromaniancommunity.Activities.models.ProfileAddPostModel;
import com.example.unitingromaniancommunity.Activities.models.ProfileFriendsModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

public class YourFriendProfileActivity extends AppCompatActivity {
    private RecyclerView recyclerView,rvProfilePost;
    private ProfileFriendsAdapter pAdapter;
    private ProfileAddPostAdapter addpostAdapter;
    private ArrayList<ProfileFriendsModel> profileFriendsModels;
    private ArrayList<ProfileAddPostModel> profileAddPostModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_friend_profile);
        recyclerView = findViewById(R.id.rv_poemName);
        rvProfilePost = findViewById(R.id.rvProfilePost);
        initAdapter();
        initAdapter1();
    }
    private void setPoemsName () {
        profileFriendsModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ProfileFriendsModel favFriendsTabModel = new ProfileFriendsModel();
            favFriendsTabModel.setUserName("Sheikh" + i);
            favFriendsTabModel.setPages(10);
            profileFriendsModels.add(favFriendsTabModel);
        }
    }

    private void initAdapter () {

        setPoemsName();
        pAdapter = new ProfileFriendsAdapter(YourFriendProfileActivity.this, profileFriendsModels);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(YourFriendProfileActivity.this ,3, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pAdapter);
    }

    private void setPoemsName1 () {
        profileAddPostModels = new ArrayList<>();
        for (int j = 1; j <= 10; j++) {
            ProfileAddPostModel profileAddPostModel = new ProfileAddPostModel();
            profileAddPostModel.setUserName("What is relaxing Day?" + j);
            profileAddPostModel.setPages(10);
            profileAddPostModels.add(profileAddPostModel);
        }
    }

    private void initAdapter1 () {

        setPoemsName1();
        addpostAdapter = new ProfileAddPostAdapter(YourFriendProfileActivity.this, profileAddPostModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(YourFriendProfileActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rvProfilePost.setLayoutManager(linearLayoutManager);
        rvProfilePost.setAdapter(addpostAdapter);
    }
}

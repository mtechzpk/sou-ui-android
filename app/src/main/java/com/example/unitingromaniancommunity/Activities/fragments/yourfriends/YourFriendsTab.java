package com.example.unitingromaniancommunity.Activities.fragments.yourfriends;


import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.unitingromaniancommunity.Activities.activities.MainActivity;
import com.example.unitingromaniancommunity.Activities.adapters.AllPostsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.FavFriendsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.FriendsTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.MessagesTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.NotifiationTabAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.YourFriendsTabAdapter;
import com.example.unitingromaniancommunity.Activities.models.AllPostsTabModel;
import com.example.unitingromaniancommunity.Activities.models.FavFriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.FriendsTabModel;
import com.example.unitingromaniancommunity.Activities.models.MessagesTabModel;
import com.example.unitingromaniancommunity.Activities.models.NotificationTabModel;
import com.example.unitingromaniancommunity.Activities.models.YourFriendsTabModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class YourFriendsTab extends Fragment {
    private RecyclerView recyclerView;
    private YourFriendsTabAdapter pAdapter;
    private ArrayList<YourFriendsTabModel> yourFriendsTabModels;
    View view;
Fragment yourFreindProfile=new YourFriendProfileFragment();
    public YourFriendsTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_your_friends_tab, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        yourFriendsTabModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            YourFriendsTabModel favFriendsTabModel = new YourFriendsTabModel();
            favFriendsTabModel.setUserName("Fawad" + i);
            favFriendsTabModel.setPages(10);
            yourFriendsTabModels.add(favFriendsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new YourFriendsTabAdapter(getActivity(), yourFriendsTabModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }


//    @Override
//    public void onItemClick(int pos) {
//
//    }
}

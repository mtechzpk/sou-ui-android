package com.example.unitingromaniancommunity.Activities.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.unitingromaniancommunity.Activities.AboutUsActivity;
import com.example.unitingromaniancommunity.Activities.IOnBackPressed;
import com.example.unitingromaniancommunity.Activities.SessionManager.SessionManager;
import com.example.unitingromaniancommunity.Activities.Utilities;
import com.example.unitingromaniancommunity.Activities.activities.MainActivity;
import com.example.unitingromaniancommunity.R;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements IOnBackPressed {
    ToggleButton toggleCountries;
    View view;
    LinearLayout lang1, lang2;
    public NavController navController;
    ImageView ivDrawer;
    TabLayout tabLayout;
    private DrawerLayout drawer;
    NavigationView navigationView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews();
//        initNavigation();
        lang1.setVisibility(View.VISIBLE);
        return view;
    }

    private void initViews() {
        toggleCountries = view.findViewById(R.id.toggleCountries);
        lang1 = view.findViewById(R.id.lang1);
        lang2 = view.findViewById(R.id.lang2);
        drawer = view.findViewById(R.id.drawer_layout);
        navigationView = view.findViewById(R.id.nav_view);
        ivDrawer = view.findViewById(R.id.ivDrawer);
        toggleCountries.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    lang1.setVisibility(View.GONE);
                    lang2.setVisibility(View.VISIBLE);
                } else {
                    // The toggle is disabled
                    lang2.setVisibility(View.GONE);
                    lang1.setVisibility(View.VISIBLE);
                }
            }
        });
    }

//    private void initNavigation() {
//        ivDrawer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                drawer.openDrawer(GravityCompat.START, true);
//            }
//        });
//        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
////        NavigationUI.setupWithNavController(bottomNavigationView, navController);
//        NavigationUI.setupWithNavController(navigationView, navController);
//        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
//            @Override
//            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                if (destination.getLabel() != null) {
//                    if (destination.getLabel().equals("dfdfd")) {
////                        rlToolbar.setVisibility(View.GONE);
//                    } else {
////                        rlToolbar.setVisibility(View.VISIBLE);
////                        tvTitle.setText(destination.getLabel());
//                    }
//                }
//
//            }
//        });
//        navigationView.getMenu().findItem(R.id.about_us).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//
//                startActivity(new Intent(getContext(), AboutUsActivity.class));
//                return true;
//            }
//        });
//        navigationView.getMenu().findItem(R.id.sign_out).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//
//                showCustomDialog();
//                return true;
//            }
//        });
//    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(getActivity());
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                SessionManager sessionManager = new SessionManager(getActivity());
                                sessionManager.logoutUser();
                                Utilities.clearSharedPref(getActivity());
                                Toast.makeText(getActivity(), "Logout", Toast.LENGTH_SHORT).show();

                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    @Override
    public boolean onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

        else if (!navController.getCurrentDestination().getLabel().toString().equals("homeFragment2")) {

           return  true;
        } else {

            showCustomDialog1();
        }

        return false;
    }
    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(getActivity());
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}

package com.example.unitingromaniancommunity.Activities.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.unitingromaniancommunity.Activities.LoginDetails.UpdateProfileActivity;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileAddPostAdapter;
import com.example.unitingromaniancommunity.Activities.adapters.ProfileFriendsAdapter;
import com.example.unitingromaniancommunity.Activities.models.ProfileAddPostModel;
import com.example.unitingromaniancommunity.Activities.models.ProfileFriendsModel;
import com.example.unitingromaniancommunity.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilesFragment extends Fragment {
        private RecyclerView recyclerView,rvProfilePost;
        private ProfileFriendsAdapter pAdapter;
        private ProfileAddPostAdapter addpostAdapter;
        private ArrayList<ProfileFriendsModel> profileFriendsModels;
        private ArrayList<ProfileAddPostModel> profileAddPostModels;
        Button bEditProfile;
        View view;
    public ProfilesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        rvProfilePost = view.findViewById(R.id.rvProfilePost);
        bEditProfile = view.findViewById(R.id.bEditProfile);
        bEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
            }
        });
        initAdapter();
        initAdapter1();
        return view;
    }
        private void setPoemsName () {
            profileFriendsModels = new ArrayList<>();
            for (int i = 1; i <= 10; i++) {
                ProfileFriendsModel favFriendsTabModel = new ProfileFriendsModel();
                favFriendsTabModel.setUserName("Sheikh" + i);
                favFriendsTabModel.setPages(10);
                profileFriendsModels.add(favFriendsTabModel);
            }
        }

        private void initAdapter () {

            setPoemsName();
            pAdapter = new ProfileFriendsAdapter(getActivity(), profileFriendsModels);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(pAdapter);
        }

        private void setPoemsName1 () {
            profileAddPostModels = new ArrayList<>();
            for (int j = 1; j <= 10; j++) {
                ProfileAddPostModel profileAddPostModel = new ProfileAddPostModel();
                profileAddPostModel.setUserName("What is relaxing Day?" + j);
                profileAddPostModel.setPages(10);
                profileAddPostModels.add(profileAddPostModel);
            }
        }

        private void initAdapter1 () {

            setPoemsName1();
            addpostAdapter = new ProfileAddPostAdapter(getActivity(), profileAddPostModels);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
            rvProfilePost.setLayoutManager(linearLayoutManager);
            rvProfilePost.setAdapter(addpostAdapter);
        }

    }

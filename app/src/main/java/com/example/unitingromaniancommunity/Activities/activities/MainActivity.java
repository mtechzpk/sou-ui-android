package com.example.unitingromaniancommunity.Activities.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.unitingromaniancommunity.Activities.AboutUsActivity;
import com.example.unitingromaniancommunity.Activities.LoginDetails.CreateBusinessProfileActivity;
import com.example.unitingromaniancommunity.Activities.PaymentActivity.PaymentInformationActivity;
import com.example.unitingromaniancommunity.Activities.PostActivities.PostEventActivity;
import com.example.unitingromaniancommunity.Activities.PostActivities.PostJobActivity;
import com.example.unitingromaniancommunity.Activities.SessionManager.SessionManager;
import com.example.unitingromaniancommunity.Activities.Utilities;
import com.example.unitingromaniancommunity.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class MainActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer, ivDrawer1;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar, rlHome, rlToolbar1;
    TextView tvTitle;
    SearchView svSearch;
    ImageView ivlogo, ivBackGround, ivLogo1;
    FrameLayout frameLayout;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rlToolbar = findViewById(R.id.rlToolbar);
//        tvTitle = findViewById(R.id.tvTitle);
        drawer = findViewById(R.id.drawer_layout);
        ivDrawer1 = findViewById(R.id.ivDrawer1);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.ivDrawer);
        ivBackGround = findViewById(R.id.img_background);
        ivlogo = findViewById(R.id.ivlogo);
        svSearch = findViewById(R.id.svSearch);
        rlToolbar1 = findViewById(R.id.rlToolbar1);
//        tvTitle = findViewById(R.id.tvTitle);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
//        HomeFragment homeFragment=new HomeFragment();
//        replaceFragment(homeFragment,false,false);
//        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initNavigation();

    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        ivDrawer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    if (destination.getLabel().equals("Home")) {
                        rlToolbar1.setVisibility(View.VISIBLE);
                        rlToolbar.setVisibility(View.GONE);
                    } else if(destination.getLabel().equals("fragment_profile")) {
                        rlToolbar.setVisibility(View.GONE);
                        rlToolbar1.setVisibility(View.GONE);
//                        tvTitle.setText(destination.getLabel());
                    }
                    else{
                        rlToolbar.setVisibility(View.VISIBLE);
                        rlToolbar1.setVisibility(View.GONE);
                    }
                }
                navigationView.getMenu().findItem(R.id.about_us).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                        return true;
                    }
                });
                navigationView.getMenu().findItem(R.id.post_edit_profile).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        startActivity(new Intent(MainActivity.this, CreateBusinessProfileActivity.class));
                        return true;
                    }
                });
                navigationView.getMenu().findItem(R.id.post_edit_job).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        startActivity(new Intent(MainActivity.this, PostJobActivity.class));
                        return true;
                    }
                });
                navigationView.getMenu().findItem(R.id.post_edit_event).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        startActivity(new Intent(MainActivity.this, PostEventActivity.class));
                        return true;
                    }
                });
                navigationView.getMenu().findItem(R.id.sign_out).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        showCustomDialog();
                        return true;
                    }
                });
                navigationView.getMenu().findItem(R.id.payment_info).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        startActivity(new Intent(MainActivity.this, PaymentInformationActivity.class));
                        return true;
                    }
                });
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

        else if (!navController.getCurrentDestination().getLabel().toString().equals("homeFragment2")) {

            super.onBackPressed();
        } else {

            showCustomDialog1();
        }
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(MainActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                SessionManager sessionManager = new SessionManager(MainActivity.this);
                                sessionManager.logoutUser();
                                Utilities.clearSharedPref(MainActivity.this);
                                Toast.makeText(MainActivity.this, "Logout", Toast.LENGTH_SHORT).show();

                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(MainActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

}
